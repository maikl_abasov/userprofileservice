<?php

use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\ChatMessengerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\GroupController;

Route::post('/sanctum/register', [AuthController::class, 'register'])->name('user.register');
Route::post('/sanctum/login', [AuthController::class, 'login'])->name('user.login');
Route::get('/email/verify', [VerifyEmailController::class, 'verify'])->name('verification.verify');

// ---- Сброс пароля ----
Route::post('/forgot-password', [AuthController::class, 'resetPassword'])->name('password.email');

Route::get('/reset-password/{token}', function ($token, Request $request) {
    $email = $request->get('email');
    return view('auth.reset-password', ['token' => $token, 'email' => $email]);
})->name('password.reset');

Route::post('/reset-password', [AuthController::class, 'resetPasswordChange'])->name('password.update');
// ---- Сброс пароля ----

Route::get('/users/list', [UserController::class, 'getUsers'])->name('users.list');

Route::middleware(['auth:sanctum'])->group(function() {

    Route::prefix('users')->group(function () {

        Route::get('/current-user', [UserController::class, 'getCurrentUser'])->name('user.current-user');
        Route::post('/update', [UserController::class, 'update'])->name('user.update');
        Route::post('/change-avatar', [UserController::class, 'changeAvatar'])->name('user.avatar');
        Route::get('/item/{user_id}', [UserController::class, 'getUser'])->name('user.item');
        Route::delete('/delete/{user_id}', [UserController::class, 'deleteUser'])->name('user.delete');
        Route::post('/logout', [AuthController::class, 'logout'])->name('user.logout');
        Route::post('/photo-loader', [UserController::class, 'photoLoader'])->name('user.photo-loader');

        Route::post('/change-password', [AuthController::class, 'changePassword'])->name('user.change-password');
        Route::post('/change-email', [AuthController::class, 'changeEmail'])->name('user.change-email');

    });

    Route::post('/subscribe/add-link', [UserController::class, 'addSubscribeLink'])->name('add.subscribe.link');
    Route::delete('/subscribe/delete-link/{id}', [UserController::class, 'deleteSubscribeLink'])->name('delete.subscribe.link');

    Route::prefix('web-chat')->group(function () {

        Route::post('/add-message', [ChatMessengerController::class, 'addMessage'])->name('chat.add-message');
        Route::post('/add-message-file', [ChatMessengerController::class, 'addMessageFile'])->name('chat.add-message-file');
        Route::get('/user-messages/{from}/{to}', [ChatMessengerController::class, 'getUserMessages'])->name('chat.user-messages');
        Route::get('/group-messages/{from}/{to}', [ChatMessengerController::class, 'getGroupMessages'])->name('chat.group-messages');
        Route::get('/set-read-messages/{from}/{to}', [ChatMessengerController::class, 'setReadUserMessages'])->name('chat.set-read-messages');

    });

    Route::prefix('groups')->group(function () {

        Route::post('/', [GroupController::class, 'addGroup'])->name('group.add');
        Route::get('/', [GroupController::class, 'getGroups'])->name('group.list');

    });

});

Route::post('/test-page', function(Request $request) {

    $MESSAGE_DIRECTORY = '/public/chat/files';

    // $file = $request->file('file');
    // $path   = \Illuminate\Support\Facades\Storage::putFile($MESSAGE_DIRECTORY, $file);
//    $user = \App\Models\User::find(1)->toArray();
//    $message = 'Привет:проверка каналов';
//    event(new \App\Events\PushUserAuthEvent($user, $message));

    return response()->json('test-page');
});
