<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Group;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->createUsers();

        $this->call([RoleSeeder::class]);

        $this->createUserBindRole();

        $this->createGroups();

        $this->freindLinksCreate();

    }

    public function createUsers(): void
    {
         User::factory()->create([
             'id' => 1,
             'username' => 'Иванов Сергей',
             'email'    => 'user1@mail.ru',
             'password' => '1234',
             'role'     =>  1,
             'phone' => '8-909-345-23-45',
             'avatar' => 'public/users/avatars/user-1.jpg'
         ]);

         User::factory()->create([
            'id' => 2 ,
            'username' => 'Петров Николай',
            'email'    => 'user2@mail.ru',
            'password' => '1234',
            'role'     =>  2,
            'phone' => '8-960-845-22-34',
            'avatar' => 'public/users/avatars/user-2.jpg',
        ]);

        User::factory(38)->create();
    }


    public function createGroups(): void
    {
        Group::create([
            'id' => 1 ,
            'user_id' => 1,
            'name'    => 'Группа программистов',
            'type' => 'public',
        ]);

        Group::create([
            'id' => 3 ,
            'user_id' => 1,
            'name'    => 'Любители литературы',
            'type' => 'private',
        ]);

        Group::create([
            'id' => 2 ,
            'user_id' => 2,
            'name'    => 'Сообщество любителей музыки',
            'type' => 'public',
        ]);

        Group::factory(50)->create();

    }

    public function createUserBindRole() {

        DB::table('role_user')->insert([
            'user_id' => 1,
            'role_id' => 1,
        ]);

        DB::table('role_user')->insert([
            'user_id' => 2,
            'role_id' => 2,
        ]);
    }

    public function freindLinksCreate() {

        DB::table('user_subscribe_links')->insert([
            'from_id' => 2,
            'subscriber_id' => 1,
            'from_type' => 'user',
        ]);

        DB::table('user_subscribe_links')->insert([
            'from_id' => 3,
            'subscriber_id' => 2,
            'from_type' => 'user',
        ]);

        DB::table('user_subscribe_links')->insert([
            'from_id' => 1,
            'subscriber_id' => 3,
            'from_type' => 'user',
        ]);

        DB::table('user_subscribe_links')->insert([
            'from_id' => 4,
            'subscriber_id' => 5,
            'from_type' => 'user',
        ]);

        DB::table('user_subscribe_links')->insert([
            'from_id' => 6,
            'subscriber_id' => 1,
            'from_type' => 'user',
        ]);

        DB::table('user_subscribe_links')->insert([
            'from_id' => 1,
            'subscriber_id' => 8,
            'from_type' => 'user',
        ]);

        DB::table('user_subscribe_links')->insert([
            'from_id' => 1,
            'subscriber_id' => 9,
            'from_type' => 'user',
        ]);

    }

}
