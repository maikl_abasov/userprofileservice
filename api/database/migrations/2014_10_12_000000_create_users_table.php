<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('password');

            $table->string('avatar')->nullable();
            $table->string('phone')->nullable();
            $table->string('sex')->nullable();
            $table->string('birthdate')->nullable();
            $table->string('address')->nullable();
            $table->string('profession')->nullable();
            $table->text('note')->nullable();

            $table->integer('status')->nullable();
            $table->integer('role')->nullable();

            //$table->text('public_key')->nullable();
            //$table->text('private_key')->nullable();

            $table->string('visit_start')->nullable();
            $table->string('visit_end')->nullable();

            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();

        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
