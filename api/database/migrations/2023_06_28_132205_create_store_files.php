<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('store_files', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('message_id')->nullable();
            $table->string('type')->nullable();
            $table->string('title')->nullable();
            $table->string('path')->nullable();
            $table->string('dir')->nullable();
            $table->string('ext')->nullable();
            $table->string('tag')->nullable();
            $table->string('filename')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('store_files');
    }
};
