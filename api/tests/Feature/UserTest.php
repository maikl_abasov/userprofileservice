<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class UserTest extends TestCase
{

    public string $email = 'johndoe@email.com';

    public array $registerData = [
        'username' => 'John',
        'email'    => 'johndoe@email.com',
        'password' => '1234',
        'password_confirmation' => '1234',
        'role' => 2,
        'phone' => '8-909-345-25-56',
    ];

    public $user;

    public function createUser() {
        $this->registerData['email'] = $this->email;
        $this->user = User::create($this->registerData);
        return $this->user;
    }

    public function deleteUser($email = false) {
        if($email) User::where('email', $email)->delete();
        elseif (!empty($this->user)) $this->user->delete();
    }

    public function auth() {
        $data = [
            'email' => $this->registerData['email'],
            'password' => $this->registerData['password'],
        ];
        $response = $this->post(route('user.login'), $data);
        return $response->json();
    }

    public function initUser() {
        $this->createUser();
        return $this->auth();
    }


    public function testGetUsers(): void
    {

        $data = $this->initUser();
        $token = $data['token'];
        $response = $this->withHeaders(['Authorization' => 'Bearer '. $token])
                         ->json('GET', route('users.list'));
        $this->deleteUser();
        $response->assertStatus(200);
        $result = $response->json();
        $this->assertArrayHasKey('users', $result);
    }

    public function testGetUsersForChat(): void
    {
        $data = $this->initUser();
        $token = $data['token'];
        $response = $this->withHeaders(['Authorization' => 'Bearer '. $token])
                   ->json('GET', '/api/users/list?service=chat');
        $this->deleteUser();
        $response->assertStatus(200);
        $result = $response->json();
        $this->assertArrayHasKey('users', $result);
        $this->assertArrayHasKey('current_page', $result['users']);
    }

    public function testGetUser(): void
    {

        $user = $this->createUser();
        $response = $this->actingAs($user)->json('GET', route('user.item', $user->id));
        $this->deleteUser();
        $response->assertStatus(200);
        $this->assertArrayHasKey('user', $response->json());
    }

    public function testDeleteUser(): void
    {
        $data  = $this->initUser();
        $token = $data['token'];
        $id    = $data['user']['id'];
        $response = $this->withHeaders(['Authorization' => 'Bearer '. $token])
                         ->json('delete', route('user.delete', $id));
        $response->assertStatus(200);
        $result = $response->json();
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals(1, $result['status']);
    }

    public function testUserUpdate(): void
    {

        $data = $this->initUser();
        $token = $data['token'];
        $user  = $data['user'];

        $newName = 'Fillip Morris';
        $user['username'] = $newName;

        $response = $this->withHeaders(['Authorization' => 'Bearer '. $token])
            ->json('POST', route('user.update'), $user);
        $this->deleteUser();

        $response->assertStatus(200);
        $result = $response->json();
        $this->assertArrayHasKey('status', $result);
        $this->assertArrayHasKey('user', $result);
        $this->assertEquals($result['user']['username'], $newName);

    }

    public function testUserAvatarLoad(): void
    {

        $data  = $this->initUser();
        $token = $data['token'];
        $id    = $data['user']['id'];

        $data = [
            'avatar' => UploadedFile::fake()->image('avatar.jpg'),
            'user_id' => $id,
        ];

        $response = $this
            ->withHeaders(['Authorization' => 'Bearer '. $token])
            ->json('POST', route('user.avatar'), $data);

        $this->deleteUser();

        $response->assertStatus(200);
        $result = $response->json();
        $this->assertArrayHasKey('status', $result);
        $this->assertArrayHasKey('path', $result);
        $this->assertEquals(1, $result['status']);

        $filePath = $result['path'];
        Storage::delete($filePath);
        $path = explode('/', $filePath);
        array_pop($path);
        $path = implode('/', $path);

        Storage::deleteDirectory($path);
    }

}
