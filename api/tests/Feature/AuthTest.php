<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{

    use DatabaseTransactions;
    use WithFaker;

    public string $email = 'johndoe@email.com';

    public array $registerData = [
        'username' => 'John',
        'email'    => 'johndoe@email.com',
        'password' => '123456',
        'password_confirmation' => '123456',
        'role' => 2,
        'phone' => '8-909-345-25-56',
    ];

    public $user;

    public function createUser() {
        $this->registerData['email'] = $this->email;
        $this->user = User::create($this->registerData);
        return $this->user;
    }

    public function deleteUser($email = false) {
        if($email) User::where('email', $email)->delete();
        elseif (!empty($this->user)) $this->user->delete();
    }

    public function auth() {
        $this->createUser();
        $data = [
            'email' => $this->registerData['email'],
            'password' => $this->registerData['password'],
        ];
        $response = $this->post(route('user.login'), $data);
        return $response->json();
    }

    /////////////////////////////////
    // --- Старт тестов ------------

    // успешная регистрация
    public function testRegisterSuccess(): void
    {
        $data = $this->registerData;
        $email = $this->email;

        $response = $this->post(route('user.register'), $data);
        $this->deleteUser($email);
        $result = $response->json();
        $response->assertStatus(200);
        $this->assertArrayHasKey('token', $result);
        $this->assertArrayHasKey('user', $result);
    }

    // проверка валидации
    public function testRegisterCheckErrorValidate(): void
    {
        $data = $this->registerData;
        $data['username'] = ''; // 'Имя обязательное поле!'
        $data['password'] = '45'; // 'Пароль не меньше 6 символов!'
        $response = $this->post(route('user.register'), $data, ['Accept' => 'application/json']);
        $errors = $response['errors'];
        $response->assertStatus(422);
        $this->assertSame($errors['username'][0], 'Имя обязательное поле!');
        $this->assertSame($errors['password'][0], 'Пароль не меньше 6 символов!');
        $this->assertSame($errors['password'][1], 'Пароли не совпадают!');
    }

    // проверка уникальности почты
    public function testRegisterEmailUnique(): void
    {
        $this->createUser();
        $data  = $this->registerData;
        $response = $this->post(route('user.register'), $data, ['Accept' => 'application/json']);
        $this->deleteUser();
        $errors = $response['errors'];
        $response->assertStatus(422);
        $this->assertSame($errors['email'][0], 'Такой email уже существует!');
    }

    // успешная авторизация
    public function testLoginSuccess(): void
    {
        $this->createUser();
        $data = [
            'email' => $this->registerData['email'],
            'password' => $this->registerData['password'],
        ];
        $response = $this->post(route('user.login'), $data, ['Accept' => 'application/json']);
        $this->deleteUser();
        $result = $response->json();
        $response->assertStatus(200);
        $this->assertArrayHasKey('token', $result);
        $this->assertArrayHasKey('user', $result);
    }

    // проверка валидации аутентификации
    public function testLoginCheckErrorValidate(): void
    {
        $this->createUser();
        $data = [
            'email' => '',
            'password' => $this->registerData['password'],
        ];
        $response = $this->post(route('user.login'), $data, ['Accept' => 'application/json']);
        $this->deleteUser();
        $errors = $response['errors'];
        $response->assertStatus(422);
        $this->assertSame($errors['email'][0], 'Поле email не можеть быть пустым!');
    }

    // проверяем на неправильный пароль
    public function testLoginCheckErrorPassword(): void
    {
        $this->createUser();
        $data = [
            'email' => $this->registerData['email'],
            'password' => '5555',
        ];
        $response = $this->post(route('user.login'), $data, ['Accept' => 'application/json']);
        $this->deleteUser();
        $response->assertStatus(401);
        $this->assertSame(false, $response->json()['token']);
        $this->assertSame($response->json()['message'], 'Неправильный логин или пароль!');
    }

    // проверяем на неавторизированного пользователя
    public function testCheckTokenError(): void
    {

        $user = (array)$this->createUser();
        $response = $this->json('POST', route('user.update'), $user, ['Accept' => 'application/json']);
        $this->deleteUser();
        $result = $response->json();
        $response->assertStatus(401);
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('Unauthenticated.', $result['message']);

    }

    // проверяем на неавторизированного пользователя
    public function testUserLogout(): void
    {

        $data = $this->auth();
        $token = $data['token'];
        $response = $this->withHeaders(['Authorization' => 'Bearer '. $token])
                    ->json('POST', route('user.logout'));
        $this->deleteUser();
        $result = $response->json();
        $response->assertStatus(200);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals(true, $result['status']);

    }


}
