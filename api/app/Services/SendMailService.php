<?php


namespace App\Services;


use App\Interfaces\SendMailServiceInterface;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;

class SendMailService implements SendMailServiceInterface
{
   public function send(array $data){

       $message = $data['message'];
       $toEmail = $data['email'];
       $subject = $data['subject'];

       $mail = new SendMail($message);
       $status = Mail::to($toEmail)->send($mail);

       return $status;
   }
}
