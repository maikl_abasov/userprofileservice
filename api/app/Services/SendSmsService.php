<?php


namespace App\Services;


use App\Interfaces\SendSmsServiceInterface;

class SendSmsService implements SendSmsServiceInterface
{
    protected $apiKey = '';

    public function send(array $data){

        $email   = $data['email'];
        $message = $data['message'];
        $phone   = $data['phone'];
        $subject = $data['subject'];

        // $status = true;
        $status = mail($email, $subject, $message); // пока отправляем смс в логи

        return $status;
    }
}
