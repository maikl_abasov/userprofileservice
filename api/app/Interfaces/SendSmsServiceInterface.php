<?php


namespace App\Interfaces;


interface SendSmsServiceInterface
{
   public function send(array $data);
}
