<?php


namespace App\Interfaces;

interface SendMailServiceInterface
{
    public function send(array $data);
}
