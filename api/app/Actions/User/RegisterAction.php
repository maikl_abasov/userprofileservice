<?php

namespace App\Actions\User;

use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use Lorisleiva\Actions\Concerns\AsAction;

class RegisterAction
{
    use AsAction;

    public function handle(array $data): array
    {
        $token = false;
        $code = 403;
        $message = 'Не удалось создать пользователя';

        $data['password'] = Hash::make($data['password']);
        $newUser = User::create($data);
        if (!empty($newUser)) {
            event(new Registered($newUser));
            $token =  $newUser->createToken('app_token')->plainTextToken;
            $message = 'Пользователь успешно создан';
            $code = 200;
        }

        return [
            'result' => ['token' => $token, 'user' => $newUser, 'message' => $message],
            'code'   => $code
        ];
    }
}
