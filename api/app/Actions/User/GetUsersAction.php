<?php

namespace App\Actions\User;

use App\Models\Message;
use App\Models\User;
use Lorisleiva\Actions\Concerns\AsAction;
use Illuminate\Pagination\LengthAwarePaginator;

class GetUsersAction
{
    use AsAction;
    private LengthAwarePaginator $users;

    public function handle(array $data) : LengthAwarePaginator
    {
        $userId  = $data['user_id'];
        $service = $data['service'];
        $limit   = $data['limit'];
        $search  = $data['search'];

        if($service == 'chat') {
            if($search) {
                $this->users = User::where([
                    ['users.id', '!=', $userId],
                    ['users.username', 'like', "%$search%"]
                ])->paginate($limit);
            } else {
                $this->users = User::where('users.id', '!=', $userId)->paginate($limit);
            }

            $this->getUserMessages($userId);

        } else {
            if($userId) $this->users = User::where('id', '!=', $userId)->paginate($limit);
            else $this->users = User::paginate($limit);
        }

        return $this->users;

    }

    private function getUserMessages($userId) : void
    {
        foreach ($this->users->items() as $item) {
            $condition = ['from' => $item->id, 'to' => $userId, 'read' => NULL];
            $count = Message::where($condition)->count();
            $item->unread_messages = $count;
        }
    }
}
