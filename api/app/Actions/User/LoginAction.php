<?php

namespace App\Actions\User;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Lorisleiva\Actions\Concerns\AsAction;

class LoginAction
{
    use AsAction;

    public function handle(array $data): array
    {
        $token = false;
        $code = 401;
        $message = 'Неправильный логин или пароль!';
        $user = [];

        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            $code = 200;
            $user =  Auth::user();
            $token =  $user->createToken('app_token')->plainTextToken;
            $message = 'Успешная авторизация!';
            $this->setUserVisit($user);
        }

        return [
            'result' => ['token' => $token, 'user' => $user, 'message' => $message],
            'code'   => $code
        ];
    }

    private function setUserVisit(User $user)
    {
        $user->visit_start = date("H:i d.m.y");
        $user->visit_end = NULL;
        $user->save();
    }
}
