<?php

namespace App\Actions\User;

use Illuminate\Support\Facades\Password;
use Lorisleiva\Actions\Concerns\AsAction;

class ResetPasswordAction
{
    use AsAction;

    public function handle(array $data): array
    {

        $status = $this->broker()->sendResetLink($data);

        $message = ($status === Password::RESET_LINK_SENT) ?
            'Вам отправлено письмо на почту, перейдите по ссылке в письме для изменения пароля!':
            'Не удалось сбросить пароль,попробуйте еще раз!';

        return [
            'result' => ['status' => $status, 'message' => $message, 'email' => $data['email']],
            'code'   => 200
        ];
    }

    protected function broker()
    {
        return Password::broker();
    }
}
