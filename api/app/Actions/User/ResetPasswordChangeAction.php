<?php

namespace App\Actions\User;

use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Lorisleiva\Actions\Concerns\AsAction;

class ResetPasswordChangeAction
{
    use AsAction;

    public function handle(array $data): array
    {
        $status = Password::reset(
            $data,
            function ($user, $password) {
                $data = ['password' => Hash::make($password)];
                $user->forceFill($data)->setRememberToken(Str::random(60));
                $user->save();
                event(new PasswordReset($user));
            }
        );

        $message = ($status === Password::PASSWORD_RESET) ?
            'Пароль успешно изменен!' :
            'Не удалось изменить пароль, попробуйте еще раз!';

        return [
            'result' => ['status' => $status, 'message' => $message],
            'code'   => 200
        ];
    }

}
