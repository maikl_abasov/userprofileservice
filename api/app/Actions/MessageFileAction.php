<?php

namespace App\Actions;

use App\Models\StoreFile;
use Illuminate\Support\Facades\Storage;
use Lorisleiva\Actions\Concerns\AsAction;

class MessageFileAction
{
    use AsAction;

    public function handle($file, array $inputs, string $directory): array
    {
        $path  = Storage::putFile($directory, $file);

        $data = [
            'message_id' => $inputs['message_id'],
            'type'     => $inputs['type'],
            'title'    => $inputs['title'],
            'path'     => $path,
            'dir'      => $directory,
            'filename' => $file->getFilename(),
            'ext'      => $file->extension(),
        ];

        $saveFile = StoreFile::create($data);

        return [
            'result' => ['status' => true, 'path' => $path, 'file_id' => $saveFile->id, 'dir' => $directory],
            'code'   => 200
        ];
    }
}
