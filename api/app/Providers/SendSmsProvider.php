<?php

namespace App\Providers;

use App\Interfaces\SendSmsServiceInterface;
use App\Services\SendSmsService;
use Illuminate\Support\ServiceProvider;

class SendSmsProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(SendSmsServiceInterface::class, SendSmsService::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
