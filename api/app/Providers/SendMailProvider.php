<?php

namespace App\Providers;

use App\Interfaces\SendMailServiceInterface;
use App\Services\SendMailService;
use Illuminate\Support\ServiceProvider;

class SendMailProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(SendMailServiceInterface::class, SendMailService::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
