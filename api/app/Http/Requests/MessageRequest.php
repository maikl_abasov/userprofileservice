<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessageRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function messages(): array
    {
        return [
            'from.required' => 'Нет отправителя',
            'to.required' => 'Нет получателя',
            'password.required' => 'Поле сообщения пустое',
        ];
    }

    public function rules(): array
    {
        return [
            'from'    => ['required', 'integer'],
            'to'      => ['required', 'integer'],
            'message' => ['required', 'string'],
            'type' => ['string'],
        ];
    }
}
