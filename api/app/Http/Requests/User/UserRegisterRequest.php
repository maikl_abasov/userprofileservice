<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function messages(): array
    {
        return [
            'username.required'   => 'Имя обязательное поле!',
            'username.min'        => 'Имя не меньше 3 символов!',
            'email.required'      => 'Поле "email" обязательное!',
            'email.unique'        => 'Такой email уже существует!',
            'password.required'   => 'Пароль обязательное поле!',
            'password.confirmed'  => 'Пароли не совпадают!',
            'password.min'        => 'Пароль не меньше 6 символов!',
        ];
    }

    public function rules(): array
    {
        return [
            'username'  => 'required|min:3|max:255',
            'email'     => 'required|email|unique:users,email',
            'password'  => 'required|min:6|max:255|confirmed',
        ];
    }
}
