<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class ChangeEmailReguest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function messages(): array
    {
        return [
            'email.required' => 'Email не задан!',
            'email.unique'   => 'Такой email уже существует!',
        ];
    }

    public function rules(): array
    {
        return [
            'email' => 'required|string|max:255|unique:users',
        ];
    }
}
