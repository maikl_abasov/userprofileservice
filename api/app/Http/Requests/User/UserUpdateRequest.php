<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function messages()
    {
        return [
            'username.required' => 'Имя пользователя обязательное поле',
        ];
    }

    public function rules(): array
    {
        return [
            'username'   => ['required', 'string', 'max:255'],
            'phone'      => '',
            'birthdate'  => '',
            'address'    => '',
            'profession' => '',
            'note'       => '',
            'sex'        => '',
        ];
    }
}
