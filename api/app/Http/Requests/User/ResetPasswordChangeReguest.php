<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordChangeReguest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function messages(): array
    {
        return [
            'token.required'      => 'Токен не задан!',
            'email.required'      => 'Email не задан!',
            'password.required'   => 'Новый пароль не задан!',
            'password.confirmed'  => 'Пароли не совпадают!',
            'password.min'  => 'Пароли меньше 6 символов!',
        ];
    }

    public function rules(): array
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6|confirmed',
        ];
    }
}
