<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordReguest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function messages(): array
    {
        return [
            'password.required'  => 'Пароль не задан',
            'password.confirmed' => 'Пароли не совпадает',
            'password.min'       => 'Не меньше 6 символов',
        ];
    }

    public function rules(): array
    {
        return [
            'password' => 'required|min:6|confirmed',
        ];
    }
}
