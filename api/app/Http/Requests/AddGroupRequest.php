<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddGroupRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Нет имени группы',
            'user_id.required' => 'Нет id пользователя',
        ];
    }

    public function rules(): array
    {
        return [
            'name'    => ['required', 'string'],
            'user_id' => ['required', 'integer'],
            'type'    => ['string'],
            'description' => '',
        ];
    }
}
