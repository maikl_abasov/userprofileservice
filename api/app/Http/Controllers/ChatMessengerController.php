<?php

namespace App\Http\Controllers;

use App\Actions\MessageFileAction;
use App\Http\Requests\MessageFileRequest;
use App\Http\Requests\MessageRequest;
use App\Models\Message;
use Illuminate\Http\JsonResponse;
use App\Events\PushMessageEvent;
use Illuminate\Database\Eloquent\Collection;

class ChatMessengerController extends Controller
{

    public const MESSAGE_DIRECTORY = '/public/chat/files';

    public function addMessage(MessageRequest $request): JsonResponse {
        $data = $request->validated();
        $message = Message::create($data);
        event(new PushMessageEvent($message));
        return $this->json(['status' => $message->id, 'message'   => $message], 200);
    }

    public function addMessageFile(MessageFileRequest $request, MessageFileAction $action): JsonResponse {

        $data = $request->validated();
        $file = $request->file('file');
        $response = $action->handle($file, $data, self::MESSAGE_DIRECTORY);
        return $this->json($response['result'], $response['code']);
    }

    public function getUserMessages(int $from, int $to, Message $message): JsonResponse {
        $messages = $message->getUserMessages($from, $to);
        $messages = $this->setBelongs($messages, ['files', 'sender']);
        return $this->json(['status'  => true, 'messages' => $messages,], 200);
    }

    public function getGroupMessages(int $from, int $to, Message $message): JsonResponse {
        $messages = $message->getGroupMessages($from, $to);
        $messages = $this->setBelongs($messages, ['files', 'sender']);
        return $this->json(['status'  => true, 'messages' => $messages,], 200);
    }

    public function setReadUserMessages(int $from, int $to, Message $message): JsonResponse {
        $status = $message->setReadMessages($from, $to);
        return $this->json(['status'  => $status], 200);
    }

    protected function setBelongs(Collection $data, array $belongs = []): Collection {
        foreach ($data as $item) {
           foreach ($belongs as $name) {
               $item->$name;
           }
        }
        return $data;
    }

}
