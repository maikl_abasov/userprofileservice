<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Pusher\Pusher;

class Controller extends BaseController
{
    public $pusher = null;

    use AuthorizesRequests, ValidatesRequests;

    public function jsonResult(array $data, int $code = 200, array $headers = []): JsonResponse
    {
        return $this->json($data, $code, $headers);
    }

    public function json($data, int $code = 200, array $headers = []): JsonResponse
    {
        $headers['Content-type'] = 'application/json;charset=utf-8';
        return response()->json($data, $code, $headers, JSON_UNESCAPED_UNICODE);
    }

    public function initPusher() {

        $appKey =  env('PUSHER_APP_KEY');
        $appSecret = env('PUSHER_APP_SECRET');
        $appId = env('PUSHER_APP_ID');
        $cluster = env('PUSHER_APP_CLUSTER');

        $this->pusher = new Pusher(
            $appKey,
            $appSecret,
            $appId,
            ['cluster' => $cluster, 'useTLS' => true, 'encrypted' => true]
        );
    }

    protected function sendPusher($data, string $channelName, string $eventName, string $key = null) {
        if(!$this->pusher) $this->initPusher();
        $data = ($key) ? [$key => $data] : $data;
        return $this->pusher->trigger($channelName, $eventName, $data);
    }
}
