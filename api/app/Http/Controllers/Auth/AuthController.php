<?php

namespace App\Http\Controllers\Auth;

use App\Actions\User\LoginAction;
use App\Actions\User\RegisterAction;
use App\Actions\User\ResetPasswordAction;
use App\Actions\User\ResetPasswordChangeAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\ChangeEmailReguest;
use App\Http\Requests\User\ChangePasswordReguest;
use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\ResetPasswordChangeReguest;
use App\Http\Requests\User\UserRegisterRequest;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AuthController extends Controller
{

    public function register(UserRegisterRequest $request, RegisterAction $action): JsonResponse
    {
        $data = $request->validated();
        $response = $action->handle($data);
        return response()->json($response['result'], $response['code'], [], JSON_UNESCAPED_UNICODE);
    }

    public function login(LoginRequest $request, LoginAction $action): JsonResponse
    {
        $data = $request->validated();
        $response = $action->handle($data);
        return response()->json($response['result'], $response['code'], [], JSON_UNESCAPED_UNICODE);
    }

    public function resetPassword(Request $request, ResetPasswordAction $action): JsonResponse
    {
        $data = $request->validate(['email' => 'required|email']);
        $response = $action->handle($data);
        return response()->json($response['result'], $response['code'], [], JSON_UNESCAPED_UNICODE);
    }

    public function resetPasswordChange(ResetPasswordChangeReguest $request,
                                        ResetPasswordChangeAction $action): JsonResponse
    {
        $data = $request->validated();
        $response = $action->handle($data);
        return response()->json($response['result'], $response['code'], [], JSON_UNESCAPED_UNICODE);
    }

    public function changePassword(ChangePasswordReguest $request): JsonResponse
    {
        $data = $request->validated();
        $userId = $request->user()->id;
        $user = User::find($userId);
        $data['password'] = Hash::make($data['password']);
        $user->update($data);
        return $this->success($user);
    }

    public function changeEmail(ChangeEmailReguest $request): JsonResponse
    {
        $data = $request->validated();
        $data['email_verified_at'] = null;
        $userId = $request->user()->id;
        $user = User::find($userId);
        event(new Registered($user)); // TODO: потом поменять событие
        $user->update($data);
        return $this->success($user);
    }

    public function logout(Request $request): JsonResponse
    {
        $request->user()->tokens()->delete();
        return response()->json(['status' => true]);
    }

    protected function success(User $user): JsonResponse
    {
        $tokenData = $user->createToken('app_token');
        $token = $tokenData->plainTextToken;
        return response()->json(['token' => $token, 'user' => $user, 'token_info' => $tokenData],
            200, [], JSON_UNESCAPED_UNICODE);
    }

}
