<?php


namespace App\Http\Controllers\Auth;

use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\User;

class VerifyEmailController extends Controller
{

    public function verify(Request $request)
    {

        $user = User::find($request->get('id'));

        if ($user->hasVerifiedEmail()) {
            return response()->json(['status' => true, 'message' => 'Email подтвержден'], 200);
        }

        if ($user->markEmailAsVerified()) {
            event(new Verified($user));
        }

        $response = ['status' => true, 'message' => 'Успешное подтверждение email'];

        return view('auth.email-verify-response', $response);

    }
}
