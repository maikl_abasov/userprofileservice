<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddGroupRequest;
use App\Models\Group;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function addGroup(AddGroupRequest $request): JsonResponse
    {
        $data = $request->validated();
        $group = Group::create($data);
        if (empty($group)) {
            return $this->json(['message' => 'Не удалось создать группу'], 404);
        }
        return $this->json($group);
    }

    public function getGroups(Request $request): JsonResponse
    {
        $name  = $request->get('name');
        if($name) {
            $groups = Group::where([
                ['name', 'like', "%$name%"]
            ])->get();
        } else {
            $groups = Group::all();
        }

        return $this->json($groups);
    }
}
