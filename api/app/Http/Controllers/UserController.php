<?php

namespace App\Http\Controllers;

use App\Actions\User\GetUsersAction;
use App\Http\Requests\User\UserUpdateRequest;
use App\Models\FileStore;
use App\Models\Message;
use App\Models\StoreFile;
use App\Models\User;
use App\Models\UserSubscribeLink;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public const USERS_DIRECTORY = 'public/users';

    public function update(UserUpdateRequest $request, User $user): JsonResponse
    {
        $data = $request->validated();
        $status = $user->where('id', $request->id)->update($data);
        return response()->json([
            'status' => $status,
            'user' => $user->find($request->id),
        ], 200);
    }

    // Загрузка аватара пользователя
    public function changeAvatar(Request $request): JsonResponse
    {

        $file = $request->file('avatar');
        $userId = $request->input('user_id');
        $path = $file->store(self::USERS_DIRECTORY . '/avatars');

        $user = User::find($userId);
        if ($user->avatar) Storage::delete($user->avatar);
        $user->avatar = $path;
        $status = $user->save();

        return response()->json([
            'status' => $status,
            'path' => $path,
        ], 200);
    }

    // Загрузка фото в галерею пользователя
    public function photoLoader(Request $request, User $user): JsonResponse
    {

        $photo  = $request->file('photo');
        $userId = $request->input('user_id');
        $dir    = self::USERS_DIRECTORY . '/photos';
        $path   = Storage::putFile($dir, $photo);

        $storeFile = StoreFile::create([
            'user_id' => $userId,
            'path' => $path,
            'dir' => $dir,
            'type' => 'photo'
        ]);

        return response()->json([
            'path' => $path,
            'dir' => $dir,
            'file_id' => $storeFile->id,
        ], 200);
    }

    public function getCurrentUser(Request $request): JsonResponse
    {
        $userId = $request->user()->id;
        $user = User::find($userId);
        $relations = ['friends', 'photos', 'subscribers', 'bind_groups', 'my_groups'];
        foreach ($relations as $name) $user->$name;
        return response()->json(['user' => $user], 200);
    }

    // Выбрать пользователя
    public function getUser(int $user_id, User $user): JsonResponse
    {
        $user = User::find($user_id);
        $relations = ['friends', 'photos', 'subscribers', 'bind_groups', 'my_groups'];
        foreach ($relations as $name) $user->$name;
        return response()->json(['user' => $user], 200);
    }

    // Получить всех пользователей
    public function getUsers(Request $request, GetUsersAction $action): JsonResponse
    {

        $limit =  $request->get('limit');
        $param = [
            'user_id' => $request->get('user_id'),
            'service' => $request->get('service'),
            'search'  => $request->get('search'),
            'limit'   => $limit ? $limit : 15,
        ];

        $users = $action->handle($param);
        return response()->json(['users' => $users], 200);
    }

    // Удалить пользователя
    public function deleteUser(int $user_id, User $user): JsonResponse
    {
        $status = $user->where('id', $user_id)->delete();
        return response()->json(['status' => $status], 200);
    }

    public function addSubscribeLink(Request $request): JsonResponse
    {
        $data = $request->all();
        $link = UserSubscribeLink::where($data)->first();
        $subsribe = false;
        if (empty($link->id)) {
            $subsribe = UserSubscribeLink::create($data);
        }
        return response()->json(['subscribe' => $subsribe], 200);
    }

    public function deleteSubscribeLink($id): JsonResponse
    {
        $delete = UserSubscribeLink::destroy($id);
        return response()->json(['delete-link' => $delete], 200);
    }

}
