<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSubscribeLink extends Model
{
    use HasFactory;

    protected $fillable = [
        'from_id',
        'subscriber_id',
        'from_type',
    ];

}
