<?php

namespace App\Models;

use App\Notifications\ResetPasswordNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Role;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'username',
        'email',
        'password',
        'phone',
        'role',
        'note'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        // 'email_verified_at' => 'datetime',
        'password'    => 'hashed',
        'created_at'  => 'datetime:Y-m-d H:i:s',
        'updated_at'  => 'datetime:Y-m-d H:i:s',
        'email_verified_at'  => 'datetime:Y-m-d H:i:s',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function photos()
    {
        return $this->hasMany(StoreFile::class);
    }

    public function friends()
    {
        return $this->belongsToMany(User::class, 'user_subscribe_links', 'subscriber_id', 'from_id')
            ->where('from_type', 'user');
    }

    public function subscribers()
    {
        return $this->belongsToMany(User::class, 'user_subscribe_links', 'from_id', 'subscriber_id')
            ->where('from_type', 'user');
    }

    public function bind_groups()
    {
        return $this->belongsToMany(Group::class, 'user_subscribe_links','subscriber_id', 'from_id')
               ->where('from_type', 'group');
    }

    public function my_groups()
    {
        return $this->hasMany(Group::class);
    }
}
