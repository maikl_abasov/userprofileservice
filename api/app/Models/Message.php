<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $fillable = [
        'from',
        'to',
        'message',
        'type',
    ];

    protected $casts = [
        'created_at' => 'datetime:H:i d.m.y',
        'updated_at' => 'datetime:H:i d.m.y',
    ];

    public function getUserMessages(int $from, int $to) {
        $messages = $this->messagesWhere($from, $to)
                         ->orderBy('created_at', 'desc')->get();
        return $messages;
    }

    public function getGroupMessages(int $from, int $to) {
        $condition = ['to' => $to, 'type' => 'group'];
        $messages = $this->where($condition)->orderBy('created_at', 'desc')->get();
        return $messages;
    }

    public function setReadMessages(int $from, int $to, $status = 1) {
        return $this->where(['from' => $to, 'to' => $from])
                    ->whereNull('read')
                    ->update(['read' => $status]);
    }

    protected function messagesWhere($from, $to, $type = 'user') {
       return $this->where('type', $type)
                   ->where(['from' => $from, 'to' => $to])
                   ->orWhere(function ($query) use($from, $to) {
                      $query->where(['from' => $to, 'to' => $from]);
                  });
    }

    public function sender()
    {
        return $this->hasOne(User::class, 'id', 'from');
    }

    public function files()
    {
        return $this->hasMany(StoreFile::class, 'message_id', 'id');
    }
}
