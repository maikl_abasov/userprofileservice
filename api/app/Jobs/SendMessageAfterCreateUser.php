<?php

namespace App\Jobs;

use App\Interfaces\SendMailServiceInterface;
use App\Interfaces\SendSmsServiceInterface;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendMessageAfterCreateUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private User $user;
    private array $data;
    private SendMailServiceInterface $mailService;
    private SendSmsServiceInterface $smsService;

    public function __construct(User $user, array $data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    public function handle(SendMailServiceInterface $mailService, SendSmsServiceInterface $smsService): void
    {

        $this->mailService = $mailService;
        $this->smsService = $smsService;

        $mailStatus = $this->sendMail();
        $smsStatus = $this->sendSms();

    }

    private function sendSms() {

        $message = $this->data['sms_message'];
        $subject = $this->data['subject'];
        $phone = $this->user->phone;
        $email = $this->user->email;
        if(!$phone) return false;

        $smsParams = [
            'email'    => $email,
            'phone'    => $phone,
            'message'  => $message,
            'subject'  => $subject,
        ];

        return $this->smsService->send($smsParams);
    }

    private function sendMail() {

        $message = $this->data['mail_message'];
        $subject = $this->data['subject'];
        $email   = $this->user->email;

        $mailParams = [
            'email'    => $email,
            'message'  => $message,
            'subject'  => $subject,
        ];

        return $this->mailService->send($mailParams);
    }
}
