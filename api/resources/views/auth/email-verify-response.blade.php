@extends('layout')

@section('content')
    <main class="login-form">
        <div class="cotainer">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"> Верификация email </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    @if ($status)
                                        <div class="text-danger" style="text-align: center; color: green !important">{{ $message }}</div>
                                    @else
                                        <div class="text-danger" style="text-align: center;">{{ $message }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
