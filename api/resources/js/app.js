import { createApp } from 'vue';
import { createPinia } from 'pinia'
import Router from '@/router/router.js';
import App from '@/App.vue';
// import './bootstrap';

import SiteLayout    from '@/components/SiteLayout.vue';
import SetShowAvatar from '@/components/common/SetShowAvatar.vue';
import ModalWrap     from '@/components/common/ModalWrap.vue';
import FileLoader    from '@/components/common/FileLoader.vue';

import '@/assets/plugins/global/plugins.bundle.css';
import '@/assets/css/style.bundle.css';
import '@/assets/style.css';

const pinia = createPinia()
const app = createApp(App)
app.use(Router)
app.use(pinia)
app.component('site-layout', SiteLayout)
app.component('set-show-avatar', SetShowAvatar)
app.component('modal-wrap', ModalWrap)
app.component('file-loader', FileLoader)
app.mount('#app');
