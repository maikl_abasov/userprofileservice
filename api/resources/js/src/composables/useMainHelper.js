import {useRoute, useRouter} from "vue-router";
import tineEmitter from 'tiny-emitter/instance'
import API_ROUTES from "@/api/http.routes.js";
import { useUserStore } from '@/store/user.js';
import { useMessengerStore } from '@/store/messenger.js';
import useMessenger from '@/composables/useMessenger.js';

function useMainHelper() {

    const route = useRoute();
    const router = useRouter();
    const userStore = useUserStore();
    const messengerStore = useMessengerStore()
    const emit = (key, data = {}) => tineEmitter.emit(key, data)
    const event = (key, action = null) => tineEmitter.on(key, action)
    const {  selectItem, getMessages } = useMessenger()

    const setFileUrl = (path = null ) => {
        if(!path) return false;
        const trimString = (line, value) => line.replace(value, '');
        const storageUrl = API_ROUTES.getStorageUrl();
        path = trimString(path, 'public') ;
        return storageUrl + path;
    }

    const modalToggle = (modal_id, action = 'show') => {
        $('#' + modal_id).modal(action)
    }

    const sendRoute = (funcName, data = null, success = null, error = null, catchFn = null) => {

        if(!success && !error && !catchFn) {
            return  API_ROUTES[funcName](data);
        }

        API_ROUTES[funcName](data).then(response => {
            const { status, data } = response
            if(status) if(success) success(data)
            else if(error) error(response)
        }).catch(errData => {
            if(catchFn) catchFn(errData)
            else if(error) error(errData)
        })
    }

    return {
        route, router,
        emit, event, emitter: tineEmitter,

        API_ROUTES,
        userStore,
        messengerStore,
        useMessenger,

        setFileUrl,
        selectItem,
        getMessages,
        modalToggle,
        sendRoute,

    }
}

export default useMainHelper;
