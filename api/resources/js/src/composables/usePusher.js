import tineEmitter from 'tiny-emitter/instance'
import Pusher from "pusher-js";
import http from '@/api/http.js';

function usePusher(channelName = 'chat-messenger-channel') {

    const tokenName = "api-token";
    const PUSHER_KEY = '6a268d39f6dc871c7ff4';

    const {BASE_URL} = http();
    const broadcastUrl = BASE_URL + '/broadcasting/auth';
    const token = localStorage.getItem(tokenName);
    const headers = {headers: {Authorization: 'Bearer ' + token}}

    const pusher = new Pusher(PUSHER_KEY, {
        cluster: 'eu',
    });

    const channel = pusher.subscribe(channelName);

    const listen = () => {
        channel.bind('push-message', (data) => {
            const { message } = data;
            switch (message.type) {
                case 'user'  :  tineEmitter.emit('USER-NEW-MESSAGE', message); break;
                case 'group' :  tineEmitter.emit('GROUP-NEW-MESSAGE', message); break;
            }
        });
    }

    listen();

    return {
        pusher,
        channel,
        headers,
        PUSHER_KEY,
    }
}

export default usePusher;
