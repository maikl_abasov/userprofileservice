import {ref} from "vue";
import tineEmitter from 'tiny-emitter/instance';
import Pusher from "pusher-js";

import Echo from 'laravel-echo';
import { useUserStore } from '@/store/user.js';
import http from '@/api/http.js';

const PusherEcho = ref(null);

function usePusherEcho(name = 'presence-user-room-channel') {

    const pusherKey = '6a268d39f6dc871c7ff4';
    const { BASE_URL, TOKEN_NAME } = http();

    const broadcastUrl = BASE_URL + '/broadcasting/auth';
    const token = localStorage.getItem(TOKEN_NAME)
    const headers = {headers: {Authorization: 'Bearer ' + token}}

    const userStore   = useUserStore();
    const channelName = name;

    const init = () => {
        let headers = createAuthHeaders()
        PusherEcho.value = new Echo({
            broadcaster: 'pusher',
            cluster: 'eu',
            forceTLS: true,
            key: pusherKey,
            authEndpoint: broadcastUrl,
            auth: headers,
        });
    }

    const getToken = () => {
        let token = localStorage.getItem(TOKEN_NAME)
        return (token) ? token : null
    }

    const createAuthHeaders = () => {
        const token = localStorage.getItem(TOKEN_NAME)
        const headers = { headers: {Authorization: 'Bearer ' + token}}
        return headers;
    }

    const listen = () => {

        if(!getToken()) return false;

        init();

        PusherEcho.value.join(channelName)
            .here(members => { //--- Вы вошли в систему
                userStore.members = members
                userStore.usersOnlineFiltered();

            }).joining(user => { //--- Вошел новый пользователь
                if(user?.id) userStore.members.push(user)
                userStore.usersOnlineFiltered();
                tineEmitter.emit('ADD-NEW-MEMBER', user);

            }).leaving(user => { //--- Пользователь вышел из системы
                userStore.deleteMember(user)
                userStore.usersOnlineFiltered();
                tineEmitter.emit('DELETE-MEMBER', user);

            }).listen('user-init-action', (event) => { //--- Слушаем событие
                console.log('user-init-action', event);

            }).listenForWhisper('typing', (e) => {
                console.log(e);
                //alert(e.name)
            });

        // PusherEcho.value.channel(channelName).pusher.connection.

    }

    const exit = () => {

        PusherEcho.value.leave(channelName);
        PusherEcho.value = null;
    }

    // ------ CREATED -----

    return {
        exit,
        listen,
    }
}

export default usePusherEcho;
