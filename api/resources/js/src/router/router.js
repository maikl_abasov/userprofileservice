import { createRouter, createWebHistory } from 'vue-router';

import Home from '@/pages/Home.vue';
import Profile from '@/pages/Profile.vue';
import Register from '@/pages/Auth/Register.vue';
import Login from '@/pages/Auth/Login.vue';
import Logout from '@/pages/Auth/Logout.vue';
import PageMessenger from '@/pages/PageMessenger.vue';

const router = createRouter({
	history: createWebHistory(),
	linkActiveClass: 'active',
	routes : [
        {
            path: '/',
            component: Home,
        },

        {
            path: '/user/profile',
            component: Profile,
        },

        {
            path: '/user/register',
            component: Register,
        },

        {
            path: '/user/login',
            component: Login,
        },

        {
            path: '/user/logout',
            component: Logout,
        },

        {
            path: '/chat-messenger',
            component: PageMessenger,
            children: [
                {
                    path: 'user/:id',
                    component: PageMessenger,
                },

                {
                    path: 'group/:id',
                    component: PageMessenger,
                },
            ]
        },
    ]
});

export default router;
