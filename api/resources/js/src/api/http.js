import axios from "axios";
import tineEmitter from 'tiny-emitter/instance'

const BASE_URL    = 'http://laravel-user-profile';
const API_URL     = BASE_URL + '/api'
const STORAGE_URL = BASE_URL + '/storage';
const TOKEN_NAME  = 'api-token';

axios.defaults.baseURL = API_URL;
axios.defaults.headers['Content-Type'] = 'multipart/form-data';
axios.defaults.headers['Accept'] = 'application/json';
//axios.defaults.withCredentials = true;
//axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

const setToken = () => {
    let token = localStorage.getItem(TOKEN_NAME);
    if(!token) token = '';
    axios.defaults.headers['Authorization'] = 'Bearer ' + token;
}

const errorParse = (error) => {
    const request = error?.request;
    const data = {
        code   : request?.status,
        status : request?.status,
        message: request?.statusText,
        url    : request?.responseURL,
    }
    // tineEmitter.emit('SEND-HTTP-ERROR', data)
    console.log('SEND-HTTP-ERROR!!!', data);
    return data;
}

axios.interceptors.request.use( config => {
    return config;
}, error => {
    return Promise.reject(error);
});

axios.interceptors.response.use(response => {
    return { status: true, data: response.data, code: 200, }
}, (error) => {
    const data = errorParse(error)
    const errorResponse = {
        status: false,
        validate_errors: error?.response?.data?.errors,
        data,
        code: data.code,
        error
    }
    return Promise.reject(errorResponse);
});

function http() {

    const pathInfoFormat = (url, args) => {
        let ch = 0;
        let list = url.split('/')
        for (let i in list) {
            if (list[i][0] != ":") continue;
            list[i] = args[ch];
            ch++;
        }
        return list.join('/')
    }

    const getInfoFormat = (url, data) => {
        let args = [];
        for (let name in data) {
            let value = data[name];
            args.push(name + '=' + value)
        }
        if(args.length) url +='?' + args.join('&');
        return url
    }

    const dataFormat = (url, data = null) => {
       let post = null;
       if(data) {

           if(data instanceof  FormData) {
               post = data;
               return { uri: url, post }
           }

           if(!data?.path && !data?.get && !data?.post) {
               post = data;
               return { uri: url, post }
           }

           if(data?.path) url = pathInfoFormat(url, data.path); // path : ['value1', 'value2']
           if(data?.get)  url = getInfoFormat(url, data.get);   // get  : { field1: 'value1', field2: 'value2' }
           if(data?.post) post = data.post;                     // post : { post1: 'post1', post2: 'post2' }
       }

       return { uri: url, post }
    }

    const send = async (url, data = null, method = 'get', headers = null) => {
        setToken();
        const { uri, post } = dataFormat(url, data);
        const response = await axios[method](uri, post, headers)
        return response
    }

    return {
        TOKEN_NAME,
        BASE_URL,
        API_URL,
        STORAGE_URL,
        send,
    }
}

export default http;
