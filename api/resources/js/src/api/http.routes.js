import http from "@/api/http.js";
const { send, STORAGE_URL, API_URL } = http()

const preloader = (type = 'start') => {
    if(type == 'start') {

    } else {

    }
}

const make = async (param, preload = false) => {
   const { url, data, method } = param;
   if(preload) preloader('start')
   const response = await send(url, data, method)
   preloader('finish')
   return response;
}

const responseHandle = (response, success = null, error = null) => {
    if(response?.status) {
        if(success) success(response.data)
        return response.data;
    }
    if(error) error(response)
    return false
}

const usersParamsRender = (params) => {
    const { userId = 0, page = 1, limit = 15} = params
    const service = (params?.service) ? '&service=' + params.service : '';
    const search  = (params?.search)  ? '&search=' + params.search   : '';
    return `user_id=${userId}&page=${page}&limit=${limit}${service}${search}`;
}

const API_ROUTES = {

    responseHandle,

    getStorageUrl: () => { return STORAGE_URL; },
    getApiUrl: () => { return API_URL; },

    register : (data) => {
        return make({ url:'/sanctum/register', data , method:'post' });
    },

    login : (data) => {
        return make({ url:'/sanctum/login', data , method:'post' });
    },

    logout: () => {
        return make({ url: '/users/logout', method: 'post' });
    },

    resetPassword: (data) => {
        return make({ url: '/forgot-password', data, method: 'post' });
    },

    getUsers: (params = {}) => {
        let args = usersParamsRender(params)
        return make({ url: '/users/list?' + args }, false);
    },

    getGroups: (args = '') => {
        return make({ url: '/groups' + args , method: 'get' });
    },

    getCurrentUser: () => {
        return make({ url: '/users/current-user'});
    },

    // ----  messages   ----
    getUserMessages: (from, to) => {
        const data = { path: [from, to] }
        return make({ url: '/web-chat/user-messages/:from/:to', data , method: 'get' });
    },

    getGroupMessages: (from, to) => {
        const data = { path: [from, to] }
        return make({ url: '/web-chat/group-messages/:from/:to', data , method: 'get' });
    },

    addMessageFile : (data) => {
        return make({ url: '/web-chat/add-message-file', data , method: 'post' });
    },

    addMessage: (data) => {
        return make({ url: '/web-chat/add-message', data, method: 'post' });
    },
    // ---- ./ messages  ----

    addFriend: (data) => {
        return make({ url: '/subscribe/add-link', data, method: 'post' });
    },

    deleteFriend: (linkId) => {
        const data = { path: [linkId] }
        return make({ url: '/subscribe/delete-link/:link_id', data, method: 'delete' });
    },

    addGroup: (data) => {
        return make({ url: '/groups', data, method: 'post' });
    },

    setReadMessages: (from, to) => {
        const data = { path: [from, to] }
        return make({ url: '/web-chat/set-read-messages/:from/:to', data , method: 'get' });
    },

    changePassword : (data) => {
        return make({ url: '/users/change-password', data , method: 'post' });
    },

    changeEmail : (data) => {
        return make({ url: '/users/change-email', data , method: 'post' });
    },

    getUser: (userId) => {
        const data = { path: [userId] }
        return make({ url: '/users/item/:user_id', data, method: 'get' });
    },

    changeAvatar: (data) => {
        return make({ url: '/users/change-avatar', data, method: 'post' });
    },

    userUpdate: (data) => {
        return make({ url: '/users/update', data, method: 'post' });
    },

    deleteUser: (userId) => {
        const data = { path: [userId] }
        return make({ url: '/users/delete/:user_id', data , method: 'delete' });
    },

    photoLoading : (data) => {
        return make({ url: '/users/photo-loader', data , method: 'post' });
    },

    /********************

    //##################################
    // --- CHAT-MESSENGER ------------

    updateGroup: (id, data) => {
        return make({ url: '/groups/{group_id}', args:[id], data, method: 'put' });
    },

    getGroup: (id) => {
        return make({ url: '/groups/{group_id}', args:[id], method: 'get' });
    },

    deleteGroup: (id) => {
        return make({ url: '/groups/{group_id}', args:[id], method: 'delete' });
    },

     ********************/

}

export default API_ROUTES;
