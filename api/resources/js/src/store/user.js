import { defineStore } from 'pinia'
import tineEmitter from 'tiny-emitter/instance'
import API_ROUTES from "@/api/http.routes.js";
import { useRouter } from 'vue-router'

export const useUserStore = defineStore('user', {
    state: () => ({
        users: [],
        members: [],
        currentUser: {},
        authStatus: false,
    }),

    actions: {

        responseLogin(response) {
            const { token, user } = response;
            this.currentUser = user;
            this.authStatus = true,
            localStorage.setItem('api-token', token);
            localStorage.setItem('current_user', JSON.stringify(user));
        },

        responseLogout(response = null) {
            this.currentUser = {};
            this.authStatus = false,
            localStorage.removeItem('api-token');
            localStorage.removeItem('current_user');
        },

        getApiToken() {
            let token = localStorage.getItem('api-token');
            if(!token) token = null
            return token
        },

        initPage(callback = null) {
            const router = useRouter()
            let token = this.getApiToken();
            this.authStatus  = false;
            if(token) {
                let user = localStorage.getItem('current_user');
                this.authStatus  = true;
                // this.currentUser =  JSON.parse(user)
                callback(this.authStatus);
                this.getCurrentUser();
            } else {
                callback(this.authStatus)
                router.push('/user/login');
            }
        },

        getCurrentUser() {
            API_ROUTES.getCurrentUser().then(response => {
                const { status, data } = response
                this.currentUser =  data?.user;
            })
        },

        usersOnlineFiltered(list = null) {
            const users = (!list) ?  this.users : list;
            this.members.map(member => {
                for(let i in users) {
                    let online = (users[i].id == member.id) ? true : false
                    users[i].online = online
                    if(online) console.log('Пользователь online-' + users[i].username);
                }
            })
        },

        deleteMember(item) {
            let members = [];
            for(let i in this.members) {
                let member = this.members[i];
                if(member.id == item.id) continue;
                members.push(member);
            }
            this.members = members;
        },

    },

    getters: {
        //doubleCount: (state) => state.count * 2,
    },
})

