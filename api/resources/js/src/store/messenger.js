import { defineStore } from 'pinia'
import tineEmitter from 'tiny-emitter/instance'
import API_ROUTES from "@/api/http.routes.js";

export const useMessengerStore = defineStore('messenger', {

    state: () => ({
        groups: [],
        messages: [],
        selectItem: {},
        items: [],
        pageName: 'user',
        subscriberId: null,
    }),

    actions: {

        itemsActiveClear(id = null) {
            this.items.map(item => { item.is_active = (item.id == id) ? true : false;} );
        },

        logout() {
            this.messages = [];
            this.selectItem = {};
            this.subscriberId = null
        },

    },

    getters: {
        //doubleCount: (state) => state.count * 2,
    },
})
