import API_ROUTES from "@/api/http.routes.js";
import tineEmitter from 'tiny-emitter/instance';

function useMain() {

  const setFileUrl = (path = null ) => {
    if(!path) return false;
    const trimString = (line, value) => line.replace(value, '');
    const storageUrl = API_ROUTES.getStorageUrl();
    path = trimString(path, 'public') ;
    return storageUrl + path;
  }

  const sendRoute = (funcName, data = null, successFn = null, errorFn = null, catchFn = null) => {

    if(!successFn && !errorFn && !catchFn)
      return  API_ROUTES[funcName](data);

    API_ROUTES[funcName](data).then(response => {
      const { status, data } = response
      if(status) if(successFn) successFn(data)
      else if(errorFn) errorFn(response)
    }).catch(error => {
      const validateErrors = (error.error?.response?.data?.errors) ?
                              error.error?.response?.data?.errors: null;
      if(catchFn) catchFn(error, validateErrors)
      else if(errorFn) errorFn(error, validateErrors)
    })
  }

  const modalToggle = (modal_id, action = 'show') => {
    $('#' + modal_id).modal(action)
  }

  const notify = (message, type = 1) => {
     tineEmitter.emit('SEND-NOTIFY', { message, type})
  }

  return {
    setFileUrl,
    sendRoute,
    modalToggle,
    notify,
  }
}

export default useMain;
