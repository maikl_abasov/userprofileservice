import {useRoute, useRouter} from "vue-router";
import tineEmitter from 'tiny-emitter/instance'
import API_ROUTES from "@/api/http.routes.js";
import { useUserStore } from '@/store/user.js';
import { useMessengerStore } from '@/store/messenger.js';
import useMessenger from '@/composables/useMessenger.js';
import useMain from '@/composables/useMain.js';
import usePusherEcho from '@/composables/usePusherEcho.js'; // --- Сервис сообщений
import usePusher from '@/composables/usePusher.js';

function useMainImport() {

    const route = useRoute();
    const router = useRouter();
    const userStore = useUserStore();
    const messengerStore = useMessengerStore();
    const {  selectItem, getMessages } = useMessenger();

    const emit = (key, data = {}) => tineEmitter.emit(key, data);
    const event = (key, action = null) => tineEmitter.on(key, action);

    return {

        API_ROUTES,
        useMessenger,
        userStore,
        messengerStore,

        usePusherEcho,
        usePusher,
        useMain,

        route, router,
        emit, event, emitter: tineEmitter,
        selectItem, getMessages,

    }
}

export default useMainImport;
