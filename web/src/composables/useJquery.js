
function useJquery() {

    const $jq = (selector) => {
        const list = document.querySelectorAll(selector);
        const on = (fn, event = 'click') => {
            list.forEach(elem =>
                elem.addEventListener(event, () => fn(elem))
            );
        }
        return  {
            on,
        }
    }

    const $click = (selector, fn) => {
        const list = document.querySelectorAll(selector);
        list.forEach(elem =>
            elem.addEventListener('click', () => fn(elem) )
        );
    }

    const $toggle = (selector, nextSelector = null) => {
        let nextElem = null;
        const list = document.querySelectorAll(selector);
        const getNextElem = (elem) => {
            if(!nextSelector) return elem.nextElementSibling;
            let tag = selector + nextSelector;
            return document.querySelector(tag);
        }

        const toggle = (el) => {
            let display = el.style.display;
            display = (display == 'none') ? 'block' : 'none';
            el.style.display = display
        }

        list.forEach(elem => {
            nextElem = getNextElem(elem);
            nextElem.style.display = 'none';
            elem.addEventListener('click', () => {
                let next = getNextElem(elem);
                toggle(next);
            })
        });
    }

    const $list = (selector, fn) => {
        const list = document.querySelectorAll(selector);
        list.forEach(elem => fn(elem) );
    }

    const $active = (selector, fnList, active) => {
        const list = document.querySelectorAll(selector);
        const listRender = () => {
            list.forEach(elem => fnList(elem));
        }

        list.forEach(elem =>
            elem.addEventListener('click', () => {
                listRender()
                active(elem);
            })
        );
    }

    const $activeClass = (selector, activeClass) => {
        const list = document.querySelectorAll(selector);
        const listRender = () => {
            list.forEach(el => el.classList.remove(activeClass));
        }

        list.forEach(elem =>
            elem.addEventListener('click', () => {
                listRender()
                elem.classList.add(activeClass)
            })
        );
    }

    const setClassItem = (selector, activeClass, index = 0) => {
      const list = document.querySelectorAll(selector);
      const item = list[index];
      if(!item) return false
      item.classList.add(activeClass)
    }

    const itemsRender = (items, fn) => {
        for(let i in items) {
            let elem = items[i];
            fn(elem)
        }
        return items;
    }

    return {
        $jq,
        $list,
        $active,
        $activeClass,
        $click, $toggle,
        setClassItem,
        itemsRender,
    }
}

export default useJquery
