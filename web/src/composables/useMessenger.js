//import {useRoute, useRouter} from "vue-router";
//import tineEmitter from 'tiny-emitter/instance'
import API_ROUTES from "@/api/http.routes.js";
import { useUserStore } from '@/store/user.js'
import { useMessengerStore } from '@/store/messenger.js'

function useMessenger() {

    const userStore = useUserStore();
    const messengerStore = useMessengerStore()

    const responseMessages = (response) => {
        const {status, data} = response
        if(status) messengerStore.messages = data?.messages;
    }

    const getMessages = (fromId, toId) => {
        messengerStore.messages = [];
        const { getUserMessages, getGroupMessages, setReadMessages } = API_ROUTES;

        if(messengerStore.pageName == 'user') {
            setReadMessages(fromId, toId).then(response => {
                getUserMessages(fromId, toId).then(responseMessages)
            })
        } else {
            getGroupMessages(fromId, toId).then(responseMessages);
            groupUnreadMessagesLocal(toId, 'delete');
        }
    }

    const selectItem = (item, i = null) => {
        const currentUser = userStore.currentUser;
        messengerStore.selectItem = item;
        item.unread_messages = null;
        messengerStore.itemsActiveClear(item.id);
        checkSubscribeLink();
        getMessages(currentUser?.id, item.id);
    }

    const checkSubscribeLink = () => {

        messengerStore.subscriberId = null;
        const friends = userStore.currentUser.friends;
        const selectItem = messengerStore.selectItem;
        for(let i in friends) {
            let friend = friends[i];
            if(friend.id != selectItem.id) continue;
            messengerStore.subscriberId  = friend.pivot.subscriber_id;
            return true
        }
    }

    //################################
    // ------ Pusher Message----------

    const getUserMessagePusher = (message) => {

        let toId   = messengerStore.selectItem?.id
        let fromId = userStore.currentUser?.id;
        if(message.to != fromId) return false

        // пришло сообщение для currentUser и selectItem
        if (message.from == toId) {
            getMessages(fromId, toId)
            return true;
        }
        // пришло сообщение только для currentUser
        setUserUnreadMessage(message)
    }

    const setUserUnreadMessage = (message) => {
        for (let i in messengerStore.items) {
            let item = messengerStore.items[i];
            if (message.from != item.id) continue;

            let count = (item.unread_messages) ? item.unread_messages : 0;
            item.unread_messages = parseInt(count) + 1;
            return true;
        }
    }

    const getGroupMessagePusher = (message) => {

        let toId   = messengerStore.selectItem?.id
        let fromId = userStore.currentUser?.id;

        // пришло сообщение для currentUser и selectItem
        if (message.to == toId) {
            getMessages(fromId, toId)
            return true;
        }
        // пришло сообщение только для currentUser
        setGroupUnreadMessage(message)
    }

    const setGroupUnreadMessage = (message) => {
        for (let i in messengerStore.items) {
            let item = messengerStore.items[i];
            if (message.to != item.id) continue;

            let count = (item.unread_messages) ? item.unread_messages : 0;
            item.unread_messages = parseInt(count) + 1;
            groupUnreadMessagesLocal(item.id, 'add');
            return true;
        }
    }

    const groupUnreadMessagesLocal = (groupId, type = 'get') => {
        const name = 'GROUP-UNREAD-MESSAGES-COUNT';
        let data = localStorage.getItem(name);
        let list = (data) ? JSON.parse(data) : {};
        let count = null;
        switch (type) {
            case "add":
                if(list[groupId]) list[groupId]++;
                else list[groupId] = 1;
                localStorage.setItem(name, JSON.stringify(list))
                break;

            case "delete":
                if(list[groupId]) list[groupId] = null;
                localStorage.setItem(name, JSON.stringify(list))
                break;

            default :
                if(list[groupId]) count = list[groupId]
                break;
        }
        return count;
    }

    // ------ ./ Pusher Message ------
    //################################


    return {
        selectItem,
        getMessages,
        getUserMessagePusher,
        getGroupMessagePusher,
        groupUnreadMessagesLocal,
    }
}

export default useMessenger;
