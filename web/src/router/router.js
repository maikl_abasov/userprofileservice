import {createRouter, createWebHistory} from 'vue-router';

import Home from '@/pages/Home.vue';
import Profile from '@/pages/Profile.vue';
import Messenger from '@/pages/Messenger.vue';
import Login from '@/pages/auth/Login.vue';
import Logout from '@/pages/auth/Logout.vue';
import Register from '@/pages/auth/Register.vue';
import BulletinBoard from '@/pages/BulletinBoard.vue';

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },

    {
      path: '/user/profile',
      name: 'user-profile',
      component: Profile,
    },

    {
      path: '/user/login',
      name: 'login',
      component: Login,
    },

    {
      path: '/user/register',
      name: 'user-register',
      component: Register,
    },

    {
      path: '/user/logout',
      name: 'logout',
      component: Logout,
    },

    {
      path: '/chat-messenger',
      component: Messenger,
      children: [
        {
          path: 'user/:id',
          component: Messenger,
        },

        {
          path: 'group/:id',
          component: Messenger,
        },
      ]
    },

    {
      path: '/bulletin-board',
      name: 'bulletin-board',
      component: BulletinBoard,
    },
  ]
});

export default router;
