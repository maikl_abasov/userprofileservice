import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router/router.js';
const pinia = createPinia()

import '@/assets/plugins/global/plugins.bundle.css';
import '@/assets/css/style.bundle.css';
import '@/assets/style.css';

import SiteLayout from '@/components/SiteLayout.vue';
import SetShowAvatar from '@/components/common/SetShowAvatar.vue';
import FileLoader from '@/components/common/FileLoader.vue';
import ModalWrap  from '@/components/common/ModalWrap.vue';

const app = createApp(App);
app.use(router);
app.use(pinia);
app.component('site-layout', SiteLayout);
app.component('set-show-avatar', SetShowAvatar);
app.component('file-loader', FileLoader);
app.component('modal-wrap', ModalWrap);

app.mount('#app')
