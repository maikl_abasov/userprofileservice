
## Тестовый проект "Профиль пользователя"
### laravel 10 , Vujs 3 (SPA) composition api
### шаблон Metronic

### ссылка на приложение (demo)

https://chat-messenger.21090.aqq.ru/user-profile-service/web/dist

### Проект состоит из двух частей 

api/ серверная чать на laravel 10 

web/ фронт часть на vuejs 3 

 ### web/src/api/api.js  в этом файле расположен  

 BASE_URL который связывает две части web-приложения

 let BASE_URL = 'http://laravel-sites/user-profile-service/api/public';


### Для запуска

создать базу mysql и прописать в api/env

cd api/  

     composer install

     php artisan migrate --seed

     php artisan storage:link

cd web/
   
    npm install (yarn install)

    npm run serve
   

### Дополнительно

запустить тесты для серверной части

cd api/ 

    php artisan test

## routes list

GET      api/email/verify 

POST      api/forgot-password 

POST      api/reset-password 

GET|HEAD  api/reset-password/{token} 

POST      api/sanctum/login 

POST      api/sanctum/register 

POST      api/users/change-avatar 

PUT       api/users/change-email 

PUT       api/users/change-password 

GET       api/users/current-user  

DELETE    api/users/delete/{user_id}  

GET       api/users/item/{user_id}  

GET       api/users/list  

POST      api/users/logout 

POST      api/users/photo-loader

PUT       api/users/update 


